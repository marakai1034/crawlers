using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System;
using System.Linq;
using System.Text;
using VoxReader;

public class MCubesBuilder : MonoBehaviour
{
    public TextAsset voxFile;
    public bool flipNormal = false;
    public bool isRoom = true;

    public Material material;

    [Header("0 for Max Size")]
    public int chunkWidth = 0;
    public int chunkHeight = 0;
    public int chunkDepth = 0;

    private int width = 0;
    private int height = 0;
    private int depth = 0;

    SizeChunk sizeChunk;
    VoxelChunk voxelChunk;
    PaletteChunk paletteChunk;

    void Start()
    {
        ReadVoxelData();

        BuildVoxels();
    }

    void BuildVoxels()
    {
        BuildChunks();
    }

    void ReadVoxelData()
    {
        var data = voxFile.bytes;

        Chunk[] chunks = VoxReader.Reader.GetChunks(data);

        sizeChunk = chunks.FirstOrDefault(c => c.Id == nameof(VoxReader.ChunkType.SIZE)) as VoxReader.SizeChunk;
        Debug.Log(sizeChunk?.ToString());

        voxelChunk = chunks.FirstOrDefault(c => c.Id == nameof(VoxReader.ChunkType.XYZI)) as VoxReader.VoxelChunk;
        Debug.Log(voxelChunk?.ToString());

        paletteChunk = chunks.FirstOrDefault(c => c.Id == nameof(VoxReader.ChunkType.RGBA)) as VoxReader.PaletteChunk;
        Debug.Log(paletteChunk?.ToString());

        width = sizeChunk.X + 5;
        height = sizeChunk.Y + 5;
        depth = sizeChunk.Z + 5;

        if (chunkDepth <= 0)
        {
            chunkDepth = depth;
        }

        if (chunkHeight <= 0)
        {
            chunkHeight = height;
        }

        if (chunkWidth <= 0)
        {
            chunkWidth = width;
        }
    }

    void BuildChunks()
    {
        for (int x = 0; x < width; x += chunkWidth - 2 - 1)
        {
            for (int y = 0; y < height; y += chunkHeight - 2 - 1)
            {
                for (int z = 0; z < depth; z += chunkDepth - 2 - 1)
                {
                    BuildChunk(x, y, z);
                }
            }
        }
    }

    void BuildChunk(int x, int y, int z)
    {
        GameObject ne = new GameObject();

        ne.gameObject.name = "Chunk:" + x + ":" + y + ":" + z;

        ne.transform.parent = transform;

        ne.transform.localPosition = new Vector3(x, y, z);
        ne.transform.localRotation = Quaternion.identity;

        MeshFilter filter = ne.AddComponent<MeshFilter>();
        MeshRenderer renderer = ne.AddComponent<MeshRenderer>();
        MeshCollider coll = ne.AddComponent<MeshCollider>();
        MCubesChunk mCubesChunk = ne.AddComponent<MCubesChunk>();

        renderer.material = material;

        mCubesChunk.Initialize(sizeChunk , voxelChunk, paletteChunk, -x + 1, -y + 1, -z + 1, chunkWidth + 1, chunkHeight + 1, chunkDepth + 1, flipNormal, isRoom);
    }
}
