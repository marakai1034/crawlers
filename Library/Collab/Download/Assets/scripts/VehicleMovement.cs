﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VehicleMovement : MonoBehaviour
{
    public bool controlled = false;
    //public CharacterController controller;
    private Transform sub;
    private Rigidbody rb;
    private ConstantForce cf;

    [SerializeField] private Transform backRaycast;
    [SerializeField] private Transform foreRaycast;

    [Header("Config")]
    public float minRotationSpeedRatio = 0.1f;
    public float maxRotationSpeed = 5f;
    public float topSpeed = 10f;
    public float speedForce = 1000f;
    public float speedTorque = 10000f;
    public float gravityForce = 1f;
    public float groundMaxDistance = 2f;

    Vector3 velocity;
    bool isGrounded;

    void Start() {
        //controlled = false;
        rb = GetComponent<Rigidbody>();
        cf = GetComponent<ConstantForce>();

        cf.force = -Vector3.up * rb.mass * gravityForce;
    }

    void Update() {
        VehicleController();
    }

    void VehicleController()
    {
        if (controlled == true)
        {

            float y = -Input.GetAxis("Horizontal"); //forward backward
            float x = Input.GetAxis("Vertical"); //left right
            //float up = Input.GetAxis("Up");
            //float lAndR = Input.GetAxis("landr");
            //float uAndD = Input.GetAxis("uandd");

            bool isBackgroundValid = GetRayLength(backRaycast, -backRaycast.up) < groundMaxDistance;
            bool isForegroundValid = GetRayLength(foreRaycast, -foreRaycast.up) < groundMaxDistance;

            int isMovingForward = 1; //not yet implemented
            if (isForegroundValid)
            {
                rb.angularVelocity = isMovingForward * Vector3.ClampMagnitude(Vector3.Slerp(rb.angularVelocity, Vector3.up * -y * speedTorque, 0.1f), rb.velocity.magnitude * minRotationSpeedRatio);

                rb.AddRelativeForce(NormalizeSpeed(Vector3.right * x * speedForce * rb.mass / 2)); //forewheel
            }

            if (isBackgroundValid)
            {
                rb.AddRelativeForce(NormalizeSpeed(Vector3.right * x * speedForce * rb.mass / 2)); //backwheel
            }

            //Clamping Force and Rotation

            rb.angularVelocity = isMovingForward * Vector3.ClampMagnitude(rb.angularVelocity, maxRotationSpeed);
            rb.velocity = Vector3.ClampMagnitude(rb.velocity, topSpeed);

            //rb.velocity = Vector3.Lerp(rb.velocity, Vector3.zero, 0.02f);

            //Debug.Log(GetRayLength(foreRaycast, -transform.up));

            //Debug.Log(rb.velocity);
        }
    }

    Vector3 NormalizeSpeed(Vector3 vector)
    {
        return vector * Time.deltaTime;
    }

    float NormalizeSpeed(float x)
    {
        return x * Time.deltaTime;
    }

    float GetRayLength(Transform tr, Vector3 vector)
    {
        Ray ray = new Ray(tr.position, vector);
        RaycastHit raycastHit = new RaycastHit();

        if (Physics.Raycast(ray, out raycastHit, 10))
        {
            return raycastHit.distance;
        }

        return 10;
    }
}
