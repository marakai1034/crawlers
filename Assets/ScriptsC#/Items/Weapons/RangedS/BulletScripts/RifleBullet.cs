﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RifleBullet : MonoBehaviour {
    public float aliveTime;
    public float damage;
    public float moveSpeed;

    private GameObject enemyTriggered;

    void Update() {
        aliveTime -= 1 * Time.deltaTime;
        if (aliveTime <= 0) {
            Destroy(this.gameObject);
        }
        this.transform.Translate(Vector3.forward * Time.deltaTime * moveSpeed);
    }

    void OnTriggerEnter(Collider other) {
        if (other.tag == "Enemy"){
            enemyTriggered = other.gameObject;
            enemyTriggered.GetComponent<Enemy>().health -= damage;
            Destroy(this.gameObject);
        }
    }
}
