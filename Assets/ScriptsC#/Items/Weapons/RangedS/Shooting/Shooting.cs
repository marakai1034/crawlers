﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shooting : MonoBehaviour
{
    public GameObject bullet;
    public GameObject bulletSpawn;
    public ParticleSystem MuzzleFlash;
    public GameObject MuzzleFlashLight;
    public float fireRate;
    private float nextFire = 0;
    private Transform _bullet;

    void Update()
    {
        if (Input.GetMouseButton(0) && Time.time > nextFire) {
            nextFire = Time.time + fireRate;
            Fire();
        }
        else
        {
 
        }
    }

    public void Fire() {
        _bullet = Instantiate(bullet.transform, bulletSpawn.transform.position, Quaternion.identity);
        _bullet.rotation = bulletSpawn.transform.rotation;
        MuzzleFlash.Play();
    }
}
