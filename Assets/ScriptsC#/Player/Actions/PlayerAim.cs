using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAim : MonoBehaviour
{
   
   

    //Animator
    public Animator itemAnimator;

    //isAiming
    bool isAiming;

    //IsHoldingWeapon
    bool isHoldingRifle1 = true;

    //isAimingWeapon


    void Start()
    {

        

    }


    void Update()
    {
        //If Right click,Is aiming = true
        if (Input.GetMouseButton(1))
        {
            isAiming = true;
            
        }
        else
        {
            isAiming = false;
           
        }
        //If Is Holding weapon,and is Pressing Right Click,Aim weapon
        //Rifle1 Aiming
        if (isAiming == true && isHoldingRifle1 == true)
        {
            itemAnimator.SetBool("IsAimingRifle1", true);
           
        }
        else
        {
            itemAnimator.SetBool("IsAimingRifle1", false);
            
        }
        
    }
}
