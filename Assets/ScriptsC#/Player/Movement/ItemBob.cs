using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemAnimator : MonoBehaviour
{
    //Bob
    public Animator itemBob;

    //Grounded
    public Transform groundCheck;
    public LayerMask groundMask;
    bool isGrounded;
    public float groundDistance = 0.4f;
    bool PlayerIsJumping;

    void Update()
    {
        isGrounded = Physics.CheckSphere(groundCheck.position, groundDistance, groundMask);
        ItemBobAnimation();
    }

    void ItemBobAnimation()
    {
        if (Input.GetKey("w"))
        {
            itemBob.SetBool("PlayerIsWalking", true);
        }
        else
        {
            itemBob.SetBool("PlayerIsWalking", false);
            itemBob.SetBool("PlayerIsSprinting", false);
        }

        if (Input.GetKey("w") && Input.GetKey(KeyCode.LeftShift))
        {
            itemBob.SetBool("PlayerIsWalking", false);
            itemBob.SetBool("PlayerIsSprinting", true);
        }
        else
        {          
            itemBob.SetBool("PlayerIsSprinting", false);
        }

        if (isGrounded == true)
        {
            PlayerIsJumping = false;
        }
        else if (isGrounded == false)
        {
            PlayerIsJumping = true;
        }

        if (PlayerIsJumping == true)
        {
            itemBob.SetBool("PlayerIsWalking", false);
            itemBob.SetBool("PlayerIsSprinting", false);
        }
    }
}
