using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HungerSystem : MonoBehaviour
{
    private float hungerPoint = 0;
    private float thirstPoint = 0;

    private const float hungerPointMax = 100;
    private const float thirstPointMax = 100;

    private const float hungerLoss = 1.0f;
    private const float thirstLoss = 1.0f;

    private float hungerLossMultiplier = 0.1f;
    private float thirstLossMultiplier = 0.1f;

    /// <summary>
    /// Check if player is hungry
    /// </summary>
    public bool IsHungry { get { return hungerPoint < 0; }}

    /// <summary>
    /// Check if player is thirsty
    /// </summary>
    public bool IsThirsty { get { return thirstPoint < 0; }}

    void Start()
    {
        hungerPoint = hungerPointMax;
        thirstPoint = thirstPointMax;
    }

    void Update()
    {
        LossHungerPoint();
        LossThirstPoint();
    }

    /// <summary>
    /// If Eating, Call this
    /// </summary>
    /// <param name="value"></param>
    public void IncreaseHunger(float value)
    {
        hungerPoint = Mathf.Clamp(hungerPoint + value, 0, hungerPointMax);
    }

    /// <summary>
    /// If Drinking, Call this
    /// </summary>
    /// <param name="value"></param>
    public void IncreaseThirst(float value)
    {
        thirstPoint = Mathf.Clamp(thirstPoint + value, 0, thirstPointMax);
    }

    /// <summary>
    /// If there is temperature changes, or day and night, change hunger Multiplier
    /// </summary>
    /// <param name="mult"></param>
    public void ChangeHungerMultiplier(float mult)
    {
        hungerLossMultiplier = mult;
    }

    /// <summary>
    /// If there is temperature changes, or day and night, change hunger Multiplier
    /// </summary>
    /// <param name="mult"></param>
    public void ChangeThirstMultiplier(float mult)
    {
        thirstLossMultiplier = mult;
    }

    private void LossHungerPoint()
    {
        hungerPoint -= hungerLoss * hungerLossMultiplier * Time.deltaTime; //not yet implemented with pause system
    }

    private void LossThirstPoint()
    {
        thirstPoint -= thirstLoss * thirstLossMultiplier * Time.deltaTime; //not yet implemented with pause system
    }
}
