using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemBob : MonoBehaviour
{
    //Bob
    public Animator itemAnimator;

    //Grounded
    public Transform groundCheck;
    public LayerMask groundMask;
    bool isGrounded;
    public float groundDistance = 0.4f;
    bool PlayerIsJumping;

    //Aiming
    bool isAiming;
    //Rifle1
    bool Rifle1IsEquipped = true;
    bool isAimingRifle1;

    void Update()
    {
        isGrounded = Physics.CheckSphere(groundCheck.position, groundDistance, groundMask);
        ItemBobAnimation();
    }

    //Bobbing Animation
    void ItemBobAnimation()
    {
        if (Input.GetKey("w"))
        {
            itemAnimator.SetBool("PlayerIsWalking", true);
        }
        else
        {
            itemAnimator.SetBool("PlayerIsWalking", false);
            itemAnimator.SetBool("PlayerIsSprinting", false);
        }

        if (Input.GetKey("w") && Input.GetKey(KeyCode.LeftShift))
        {
            itemAnimator.SetBool("PlayerIsWalking", false);
            itemAnimator.SetBool("PlayerIsSprinting", true);
        }
        else
        {
            itemAnimator.SetBool("PlayerIsSprinting", false);
        }

        if (isGrounded == true)
        {
            PlayerIsJumping = false;
        }
        else if (isGrounded == false)
        {
            PlayerIsJumping = true;
        }

        if (PlayerIsJumping == true)
        {
            itemAnimator.SetBool("PlayerIsWalking", false);
            itemAnimator.SetBool("PlayerIsSprinting", false);
        }

        //if Aiming Dont Bob

        if (isAiming == true)
        {
            itemAnimator.SetBool("PlayerIsWalking", false);
            itemAnimator.SetBool("PlayerIsSprinting", false);
        }
        //Is Aiming
        if (Input.GetMouseButton(1))
        {
            isAiming = true;
        }
        else
        {
            isAiming = false;
        }

        //Aiming Animations

        //Rifle1
        if(isAiming == true && Rifle1IsEquipped == true)
        {
             isAimingRifle1 = true;
        }
        else
        {
            isAimingRifle1 = false;
        }

        if (isAimingRifle1 == true)
        {
            itemAnimator.SetBool("IsAimingRifle1", true);
        }
        else
        {
            itemAnimator.SetBool("IsAimingRifle1", false);
        }





    }
}
