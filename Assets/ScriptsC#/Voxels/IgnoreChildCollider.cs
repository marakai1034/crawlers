using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IgnoreChildCollider : MonoBehaviour
{
    public Collider collider;
    public Transform rootParent;
    
    void Start()
    {
        for (int i = 0; i < transform.childCount; i++)
        {
            Physics.IgnoreCollision(collider, transform.GetChild(i).gameObject.GetComponent<Collider>());
        }
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            FracturePart.currentTest++;
        }
    }
}
