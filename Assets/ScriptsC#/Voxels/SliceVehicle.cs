using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VoxelSystem;

public class SliceVehicle : MonoBehaviour
{ 
    protected Mesh mesh;
    [SerializeField] protected Material material = null;
    [SerializeField] protected int resolution = 24;
    [SerializeField] protected bool useUV = false;

    void Start()
    {
        List<Voxel_F> voxels;
        float unit;

        var filter = GetComponent<MeshFilter>();
        var mesh = filter.mesh;

        CPUVoxelizer.Voxelize(mesh, resolution, out voxels, out unit);
        VoxelMesh.Build(transform ,material,voxels.ToArray(), unit, useUV);

        GetComponent<MeshRenderer>().enabled = false;
    }
}
