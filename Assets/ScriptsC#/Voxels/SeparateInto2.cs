using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class SeparateInto2 : MonoBehaviour
{
    public MeshFilter meshFilter;
    public int iterative = 0;

    Queue<MeshFilter> meshFilterList = new Queue<MeshFilter>();
    Queue<MeshFilter> meshFilterListNew = new Queue<MeshFilter>();

    void Start()
    {
        meshFilterList.Enqueue(meshFilter);
        for (int i = 0;i < iterative; i++)
        {
            while (meshFilterList.Count > 0)
            {
                SeparateRec(meshFilterList.Dequeue());
            }
            meshFilterList = meshFilterListNew;
            meshFilterListNew = new Queue<MeshFilter>();
        }

        Destroy(meshFilter.gameObject);
    }

    void Update()
    {
        if (meshFilter)
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                //Separate();
            }
        }
    }

    void SeparateRec(MeshFilter meshFilter)
    {
        if (meshFilter)
        {
            Vector3[] vertices = meshFilter.mesh.vertices;
            int[] triangles = meshFilter.mesh.triangles;

            //Vector3[] firstVertices = vertices.Take(vertices.Length / 2).ToArray();
            //Vector3[] secondVertices = vertices.Skip(vertices.Length / 2).ToArray();

            int[] firstTriangles = triangles.Take(triangles.Length / 2).ToArray();
            int[] secondTriangles = triangles.Skip(triangles.Length / 2).ToArray();

            meshFilterListNew.Enqueue(CreateMesh(vertices, firstTriangles));
            meshFilterListNew.Enqueue(CreateMesh(vertices, secondTriangles));
        }
    }

    MeshFilter CreateMesh(Vector3[] vertices, int[] triangles)
    {
        GameObject ne = new GameObject("separated");

        ne.transform.position = transform.position;

        MeshFilter filter = ne.AddComponent<MeshFilter>();
        ne.AddComponent<MeshRenderer>();

        filter.mesh = new Mesh();
        filter.mesh.vertices = vertices;
        filter.mesh.triangles = triangles;
        filter.mesh.normals = meshFilter.mesh.normals;
        filter.mesh.uv = meshFilter.mesh.uv;
        ne.GetComponent<MeshRenderer>().materials = meshFilter.GetComponent<MeshRenderer>().materials;
        ne.transform.position = meshFilter.transform.position;
        ne.transform.rotation = meshFilter.transform.rotation;
        ne.transform.localScale = meshFilter.transform.localScale;

        return filter;
    }

    /*
    void Separate()
    {
        Vector3[] vertices = meshFilter.mesh.vertices;
        int[] triangles = meshFilter.mesh.triangles;

        Vector3[] firstVertices = vertices.Take(vertices.Length / 2).ToArray();
        Vector3[] secondVertices = vertices.Skip(vertices.Length / 2).ToArray();

        int[] firstTriangles = triangles.Take(triangles.Length / 2).ToArray();
        int[] secondTriangles = triangles.Skip(triangles.Length / 2).ToArray();

        GameObject ne = Instantiate(new GameObject("separated"));
        ne.transform.position = transform.position;

        MeshFilter filter = ne.AddComponent<MeshFilter>();
        ne.AddComponent<MeshRenderer>();

        filter.mesh = new Mesh();
        filter.mesh.vertices = vertices;
        filter.mesh.triangles = secondTriangles;
        filter.mesh.normals = meshFilter.mesh.normals;
        filter.mesh.uv = meshFilter.mesh.uv;
        ne.GetComponent<MeshRenderer>().materials = GetComponent<MeshRenderer>().materials;

        meshFilter.mesh.vertices = vertices;
        meshFilter.mesh.triangles = firstTriangles;
    }
    */
}
