using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FracturePart : MonoBehaviour
{
    public static int currentTest = -1;
    static int indexCounter = 0;
    int currentIndex = 0;

    private Rigidbody rb;
    private MeshCollider col;

    void Start()
    {
        currentIndex = indexCounter++;
        rb = gameObject.AddComponent<Rigidbody>();
        col = gameObject.AddComponent<MeshCollider>();

        rb.isKinematic = true;
        rb.useGravity = false;
        col.convex = true;
        col.isTrigger = true;
    }

    void Update()
    {
        if (currentIndex == currentTest || Input.GetKeyDown(KeyCode.Space))
        {
            rb.isKinematic = false;
            col.isTrigger = false;
            rb.useGravity = true;
            rb.transform.parent = null;
        }
    }
}
