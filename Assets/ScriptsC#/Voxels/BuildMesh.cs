using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildMesh : MonoBehaviour
{
    MeshFilter meshFilter;

    void Start()
    {
        meshFilter = GetComponent<MeshFilter>();
        BuildingMesh();
    }

    void Update()
    {
        
    }

    void BuildingMesh()
    {
        Vector3[] vertices = new Vector3[]
        {
            new Vector3(0,0,0),
            new Vector3(0,0,1),
            new Vector3(1,0,0),
            new Vector3(1,0,1)
        };

        int[] triangles = new int[]
        {
            0, 1, 2,
            1, 3, 2
        };

        Mesh mesh = new Mesh();

        meshFilter.mesh = mesh;

        mesh.Clear();
        mesh.vertices = vertices;
        mesh.triangles = triangles;

        mesh.RecalculateNormals();

    }
}
