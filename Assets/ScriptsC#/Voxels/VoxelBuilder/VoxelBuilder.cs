using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VoxelBuilder : MonoBehaviour
{
    private MeshFilter meshFilter;
    private MeshRenderer meshRenderer;

    public GameObject voxel;

    private Mesh baseMesh;
    private List<Mesh> partMesh;

    private void Awake()
    {
        meshFilter = GetComponent<MeshFilter>();
        meshRenderer = GetComponent<MeshRenderer>();

        baseMesh = meshFilter.mesh;

        partMesh = new List<Mesh>();

        HashSet<Vector3> bert = new HashSet<Vector3>();

        for (int i = 0; i < baseMesh.vertices.Length; i++)
        {
            bert.Add(baseMesh.vertices[i]);
        }

        //GameObject voxel = GameObject.CreatePrimitive(PrimitiveType.Cube);

        //float voxelSize = transform.localScale.x; //it must be cubic, and volumen
        Vector3 scale = Vector3.one * 0.1f;

        foreach (var x in bert)
        {
            GameObject ne = Instantiate(voxel, transform);
            ne.transform.localPosition = x;
            ne.transform.localScale = scale;
        }

        Destroy(meshRenderer);
        Destroy(meshFilter);

        //DestroyPart(new Vector3(), 1);
    }
}
