using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreateOneVoxel : MonoBehaviour
{
    private List<Vector3> vertices = new List<Vector3>();
    private List<int> triangles = new List<int>();

    void Start()
    {
        MeshFilter filter = GetComponent<MeshFilter>();
        AddVoxel(0,transform.position,1);
        filter.mesh = new Mesh();
        filter.mesh.SetVertices(vertices);
        filter.mesh.triangles = triangles.ToArray();

        filter.mesh.RecalculateBounds();
        filter.mesh.RecalculateNormals();
    }

    void AddVoxel(int i, Vector3 pos, float size)
    {
        //0
        vertices.Add(pos);

        //1
        vertices.Add(pos + Vector3.right * size);

        //2
        vertices.Add(pos + Vector3.right * size + Vector3.forward * size);

        //3
        vertices.Add(pos + Vector3.forward * size);

        //4
        vertices.Add(pos + Vector3.up * size);

        //5
        vertices.Add(pos + Vector3.right * size + Vector3.up * size);

        //6
        vertices.Add(pos + Vector3.right * size + Vector3.forward * size + Vector3.up * size);

        //7
        vertices.Add(pos + Vector3.forward * size + Vector3.up * size);

        int id = i * 8;

        //Top
        triangles.Add(id + 6);
        triangles.Add(id + 5);
        triangles.Add(id + 4);

        triangles.Add(id + 4);
        triangles.Add(id + 7);
        triangles.Add(id + 6);

        //Bottom
        triangles.Add(id + 0);
        triangles.Add(id + 1);
        triangles.Add(id + 2);

        triangles.Add(id + 0);
        triangles.Add(id + 2);
        triangles.Add(id + 3);

        //Right
        triangles.Add(id + 1);
        triangles.Add(id + 6);
        triangles.Add(id + 2);

        triangles.Add(id + 1);
        triangles.Add(id + 5);
        triangles.Add(id + 6);

        //Left
        triangles.Add(id + 0);
        triangles.Add(id + 7);
        triangles.Add(id + 4);

        triangles.Add(id + 0);
        triangles.Add(id + 3);
        triangles.Add(id + 7);

        //Front
        triangles.Add(id + 0);
        triangles.Add(id + 5);
        triangles.Add(id + 1);

        triangles.Add(id + 0);
        triangles.Add(id + 4);
        triangles.Add(id + 5);

        //Back
        triangles.Add(id + 3);
        triangles.Add(id + 6);
        triangles.Add(id + 7);

        triangles.Add(id + 3);
        triangles.Add(id + 2);
        triangles.Add(id + 6);
    }
}
