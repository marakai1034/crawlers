using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChunksData
{
    //5 x 5 x 5
    public bool[,,] chunks;
    public const int chunksSize = 16;

    public ChunksData()
    {
        chunks = new bool[chunksSize, chunksSize, chunksSize];
    }
}

public class Vector3Comparer : IComparer<Vector3>
{
    public int Compare(Vector3 a, Vector3 b)
    {
        int x = a.x.CompareTo(b.x);
        int y = a.y.CompareTo(b.y);
        int z = a.z.CompareTo(b.z);

        if (x != 0)
        {
            return x;
        }

        if (y != 0)
        {
            return y;
        }

        if (z != 0)
        {
            return z;
        }

        return 0;
    }
}

public class VoxelBuilderOptimize : MonoBehaviour
{
    private MeshFilter meshFilter;
    private MeshRenderer meshRenderer;

    public Material voxelMaterial;

    private Mesh baseMesh;
    //public GameObject voxel;

    private SortedSet<Vector3> sortedVertices;

    float minX = float.MaxValue, minY = float.MaxValue, minZ = float.MaxValue;
    float maxX = float.MinValue, maxY = float.MinValue, maxZ = float.MinValue;

    ChunksData[,,] chunksData;

    private void Start()
    {
        meshFilter = GetComponent<MeshFilter>();
        meshRenderer = GetComponent<MeshRenderer>();

        baseMesh = meshFilter.mesh;

        InitializeVoxels();
    }

    private void Update()
    {
        if (Input.GetKey(KeyCode.Return))
        {
            //CreateMesh();
        }
    }

    void InitializeVoxels()
    {
        SortVertex(); //Shipp
        InitializeChunks(); //Shipp
        CreateChunks(); //Shipp
        CreateEachChunk(); //Shipp
    }

    void SortVertex()
    {
        sortedVertices = new SortedSet<Vector3>(new Vector3Comparer());

        foreach (Vector3 vec in baseMesh.vertices)
        {
            sortedVertices.Add(vec);
            CompareMinMaxVertex(vec);
        }

        Debug.Log(sortedVertices.Count);

        /*
        foreach (Vector3 vec in sortedVertices) {
            CompareMinMaxVertex(vec);
        }
        */

        Debug.Log("Max X :" + maxX);
        Debug.Log("Max Y :" + maxY);
        Debug.Log("Max Z :" + maxZ);
    }

    void InitializeChunks()
    {
        int xSize = Mathf.CeilToInt((maxX - minX) * 10 / ChunksData.chunksSize) + 1; //not optimized
        int ySize = Mathf.CeilToInt((maxY - minY) * 10 / ChunksData.chunksSize) + 1; //not optimized
        int zSize = Mathf.CeilToInt((maxZ - minZ) * 10 / ChunksData.chunksSize) + 1; //not optimized

        Debug.Log("SIZE : " + xSize + " " + ySize + " " + zSize);

        chunksData = new ChunksData[xSize,ySize,zSize];

        //Fill em up
        for (int x = 0;x < xSize; x++)
        {
            for (int y = 0; y < ySize; y++)
            {
                for (int z = 0; z < zSize; z++)
                {
                    chunksData[x, y, z] = new ChunksData(); //Initializating
                }
            }
        }
    }

    void CreateChunks()
    {
        int duplicate = 0;
        foreach (Vector3 vec in sortedVertices)
        {
            //Search Chunk Index
            int xChunk = Mathf.RoundToInt((vec.x - minX) * 10) / ChunksData.chunksSize;
            int yChunk = Mathf.RoundToInt((vec.y - minY) * 10) / ChunksData.chunksSize;
            int zChunk = Mathf.RoundToInt((vec.z - minZ) * 10) / ChunksData.chunksSize;
            
            //Search Chunk Voxel's Index
            int x = Mathf.RoundToInt((vec.x - minX) * 10) % ChunksData.chunksSize;
            int y = Mathf.RoundToInt((vec.y - minY) * 10) % ChunksData.chunksSize;
            int z = Mathf.RoundToInt((vec.z - minZ) * 10) % ChunksData.chunksSize;

            //Debug.Log(xChunk + " : " + yChunk + " : " + zChunk);

            ChunksData current = chunksData[xChunk, yChunk, zChunk];

            if (current.chunks[x, y, z] == false)
            {
                current.chunks[x, y, z] = true;
            } else
            {
                duplicate++;
            }
        }

        Debug.Log("DUPLICATE : " + duplicate);
    }

    void CreateEachChunk()
    {
        bool br = false;
        for (int x = 0;x < chunksData.GetLength(0); x++)
        {
            for (int y = 0; y < chunksData.GetLength(1); y++)
            {
                for (int z = 0; z < chunksData.GetLength(2); z++)
                {
                    Vector3 pos = new Vector3(x, y, z) * ChunksData.chunksSize * 0.1f;
                    CreateChunk(pos, chunksData[x,y,z]);
                    
                    //br = true;
                    //break;
                }
                //if (br) break;
            }
            //if (br) break;
        }
    }

    void CreateChunk(Vector3 pos, ChunksData data)
    {
        List<Vector3> vertices = new List<Vector3>();
        List<int> triangles = new List<int>();
        List<Vector2> uv = new List<Vector2>();

        GameObject ne = new GameObject();

        ne.transform.position = pos;

        MeshRenderer rend = ne.AddComponent<MeshRenderer>();
        rend.material = voxelMaterial;
        MeshFilter filter = ne.AddComponent<MeshFilter>();
        MeshCollider col = ne.AddComponent<MeshCollider>();

        Mesh mesh = CreateMesh(vertices, triangles, uv, data, pos);

        filter.sharedMesh = mesh;
        col.sharedMesh = mesh;
    }

    int idd = 0;

    private Mesh CreateMesh(List<Vector3> vertices, List<int> triangles, List<Vector2> uv, ChunksData data, Vector3 pos)
    {
        idd = 0;
        Mesh mesh = new Mesh();

        for (int x = 0; x < data.chunks.GetLength(0); x++)
        {
            for (int y = 0; y < data.chunks.GetLength(1); y++)
            {
                for (int z = 0; z < data.chunks.GetLength(2); z++)
                {
                    if (data.chunks[x, y, z])
                    {
                        bool top = false;
                        bool bottom = false;
                        bool right = false;
                        bool left = false;
                        bool forward = false;
                        bool back = false;

                        //DRAW TOP
                        if (y == ChunksData.chunksSize - 1 || !data.chunks[x, y + 1, z])
                        {
                            top = true;
                        }

                        //DRAW BOTTOM
                        if (y == 0 || !data.chunks[x, y - 1, z])
                        {
                            bottom = true;
                        }

                        //DRAW RIGHT
                        if (x == ChunksData.chunksSize - 1 || !data.chunks[x + 1, y, z])
                        {
                            right = true;
                        }

                        //DRAW LEFT
                        if (x == 0 || !data.chunks[x - 1, y, z])
                        {
                            left = true;
                        }

                        //DRAW BACK
                        if (z == ChunksData.chunksSize - 1 || !data.chunks[x, y, z + 1])
                        {
                            back = true;
                        }

                        //DRAW FORWARD
                        if (z == 0 || !data.chunks[x, y, z - 1])
                        {
                            forward = true;
                        }

                        //DRAW VOXEL
                        AddVoxel(vertices, triangles, uv, new Vector3(x, y, z) * 0.1f, 0.1f, top, bottom, right, left, forward, back);
                        idd++;
                        //Instantiate(voxel, pos + new Vector3(x, y, z) * 0.1f, Quaternion.identity);
                    }
                }
            }
        }

        mesh.SetVertices(vertices);
        mesh.triangles = triangles.ToArray();
        mesh.uv = uv.ToArray();

        mesh.RecalculateBounds();
        mesh.RecalculateNormals();
        mesh.RecalculateTangents();

        return mesh;
    }

    private void AddVoxel(List<Vector3> vertices, List<int> triangles, List<Vector2> uv, Vector3 pos, float size, bool top = true, bool bottom = true, bool right = true, bool left = true, bool forward = true, bool back = true)
    {
        //0
        vertices.Add(pos);

        //1
        vertices.Add(pos + Vector3.right * size);

        //2
        vertices.Add(pos + Vector3.right * size + Vector3.forward * size);

        //3
        vertices.Add(pos + Vector3.forward * size);

        //4
        vertices.Add(pos + Vector3.up * size);

        //5
        vertices.Add(pos + Vector3.right * size + Vector3.up * size);

        //6
        vertices.Add(pos + Vector3.right * size + Vector3.forward * size + Vector3.up * size);

        //7
        vertices.Add(pos + Vector3.forward * size + Vector3.up * size);

        //UV
        uv.Add(new Vector2(0, 0));
        uv.Add(new Vector2(0, 1));
        uv.Add(new Vector2(1, 1));
        uv.Add(new Vector2(1, 0));
        uv.Add(new Vector2(0, 0));
        uv.Add(new Vector2(0, 1));
        uv.Add(new Vector2(1, 1));
        uv.Add(new Vector2(1, 0));

        int id = idd * 8;

        //Top
        if (top)
        {
            triangles.Add(id + 6);
            triangles.Add(id + 5);
            triangles.Add(id + 4);

            triangles.Add(id + 4);
            triangles.Add(id + 7);
            triangles.Add(id + 6);
        }

        //Bottom
        if (bottom)
        {
            triangles.Add(id + 0);
            triangles.Add(id + 1);
            triangles.Add(id + 2);

            triangles.Add(id + 0);
            triangles.Add(id + 2);
            triangles.Add(id + 3);
        }

        //Right
        if (right)
        {
            triangles.Add(id + 1);
            triangles.Add(id + 6);
            triangles.Add(id + 2);

            triangles.Add(id + 1);
            triangles.Add(id + 5);
            triangles.Add(id + 6);
        }

        //Left
        if (left)
        {
            triangles.Add(id + 0);
            triangles.Add(id + 7);
            triangles.Add(id + 4);

            triangles.Add(id + 0);
            triangles.Add(id + 3);
            triangles.Add(id + 7);
        }

        //Front
        if (forward)
        {
            triangles.Add(id + 0);
            triangles.Add(id + 5);
            triangles.Add(id + 1);

            triangles.Add(id + 0);
            triangles.Add(id + 4);
            triangles.Add(id + 5);
        }

        //Back
        if (back)
        {
            triangles.Add(id + 3);
            triangles.Add(id + 6);
            triangles.Add(id + 7);

            triangles.Add(id + 3);
            triangles.Add(id + 2);
            triangles.Add(id + 6);
        }
    }

    private void CompareMinMaxVertex(Vector3 vec)
    {
        if (vec.x > maxX)
        {
            maxX = vec.x;
        }

        if (vec.x < minX)
        {
            minX = vec.x;
        }

        if (vec.y > maxY)
        {
            maxY = vec.y;
        }

        if (vec.y < minY)
        {
            minY = vec.y;
        }

        if (vec.z > maxZ)
        {
            maxZ = vec.z;
        }

        if (vec.z < minZ)
        {
            minZ = vec.z;
        }
    }
}
