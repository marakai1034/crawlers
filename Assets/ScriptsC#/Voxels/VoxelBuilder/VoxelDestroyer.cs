using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VoxelDestroyer : MonoBehaviour
{
    private MeshFilter meshFilter;
    private MeshCollider meshCollider;

    public GameObject voxel;

    private Mesh baseMesh;
    private Mesh bodyMesh;
    private List<Mesh> partMesh;

    private void Start()
    {
        meshFilter = GetComponent<MeshFilter>();
        meshCollider = GetComponent<MeshCollider>();

        baseMesh = meshFilter.mesh;
        bodyMesh = baseMesh;

        partMesh = new List<Mesh>();

        HashSet<Vector3> bert = new HashSet<Vector3>();

        for (int i = 0;i < bodyMesh.vertices.Length; i++)
        {
            bert.Add(bodyMesh.vertices[i]);
        }

        foreach (var x in bert)
        {
            Instantiate(voxel, x, Quaternion.identity);
        }

        //DestroyPart(new Vector3(), 1);
    }

    private void DestroyPart(Vector3 position, float force)
    {
        Mesh mesh = MakeMeshPart(position, force);

        GameObject ne = new GameObject("DestroyedPart");
        MeshFilter fe = ne.AddComponent<MeshFilter>();
        fe.mesh = mesh;

        MeshRenderer re = ne.AddComponent<MeshRenderer>();
    }

    private Mesh MakeMeshPart(Vector3 position, float force)
    {
        Vector3 point = transform.InverseTransformPoint(position);

        //List<int> verticesPartIndex = new List<int>(); //new mesh part index

        List<Vector3> verticesPart = new List<Vector3>(); //new mesh part
        List<int> trianglesPart = new List<int>(); //new mesh part

        int currentIndex = 0;
        for (int i = 0;i < bodyMesh.triangles.Length; i++)
        {
            if (Vector3.Distance(bodyMesh.vertices[bodyMesh.triangles[i]], point) < force)
            {
                verticesPart.Add(bodyMesh.vertices[bodyMesh.triangles[i]]);
                trianglesPart.Add(currentIndex++);
            }
        }

        /*
        for (int i = 0;i < bodyMesh.vertices.Length; i++)
        {
            if (Vector3.Distance(bodyMesh.vertices[i], point) < force) //if part of the mesh less than force magnitude
            {
                verticesPartIndex.Add(i);
                verticesPart.Add(bodyMesh.vertices[i]);
            }
        }

        for (int i = 0;i < trianglesPart.Count;i += 3)
        {
            for (int j = 0;j < verticesPartIndex.Count;j++)
            {

            }
            //if (trianglesPart[i] == )
            //{

            //}
        }
        */

        Mesh mesh = new Mesh();
        mesh.vertices = verticesPart.ToArray();
        mesh.triangles = trianglesPart.ToArray();

        return mesh;
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.tag == "Destroyer")
        {
            Debug.Log("Kill");
            //DestroyPart(collision.contacts[0].point , collision.relativeVelocity.magnitude);
        }
    }
}
