using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuestSystem : MonoBehaviour
{
    public static QuestSystem Instance;

    private List<Quest> quests;
    public Quest currentQuest { get; private set;}

    private void Awake()
    {
        Instance = this;
        LoadQuestData();
    }

    private void Update()
    {
        CheckQuest();
    }

    private void LoadQuestData()
    {
        if (quests == null)
        {
            quests = new List<Quest>();
        } else
        {
            quests.Clear();
        }

        //Load : Not implemented yet
        quests.Add(new Quest1());
        //THis is test

        currentQuest = quests[0];
    }

    public void AddQuest(Quest quest)
    {
        quests.Add(quest);
    }

    public void RemoveQuest(Quest quest)
    {
        quests.Add(quest);
    }

    public void CheckQuest()
    {
        if (quests != null)
        {
            for (int i = 0;i < quests.Count; i++)
            {
                quests[i].CheckQuest();
            }
        }
    }
}
