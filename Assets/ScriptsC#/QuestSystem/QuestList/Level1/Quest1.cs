using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Quest1 : Quest
{
    public Quest1()
    {
        this.questName = "Quest1";
        this.questDesc = "This is Quest 1";

        objectiveList = new List<Objective>();

        InitializeObjective();
    }

    private void InitializeObjective()
    {
        objectiveList.Add(new Quest1Obj1());
        objectiveList.Add(new Quest1Obj2());
    }
}
