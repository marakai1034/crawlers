using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Quest1Obj2 : Objective
{
    public Quest1Obj2()
    {
        objectiveDesc = "You must finish him";

        objectiveIndicator = new ObjectiveIndicator();
        objectiveIndicator.SetBooleanIndicator();
    }

    protected override void InitializeObjective()
    {
        base.InitializeObjective();
        //Inisialisasi
        Debug.Log("Quest 1 Objective 2 Initialize");
    }

    protected override void EndObjective()
    {
        base.EndObjective();
        //If finish do this
        Debug.Log("Quest 1 Objective 2 Completed");
    }

    protected override bool CheckingObjective()
    {
        //Check Objective Condition here
        if (Input.GetKeyDown(KeyCode.Return))
        {
            return true;
        }
        return false;
    }
}
