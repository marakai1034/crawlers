using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Quest1Obj1 : Objective
{
    public Quest1Obj1()
    {
        objectiveDesc = "You must kill him";

        objectiveIndicator = new ObjectiveIndicator();
        objectiveIndicator.SetBooleanIndicator();
    }

    protected override void InitializeObjective()
    {
        base.InitializeObjective();
        //Inisialisasi
        Debug.Log("Quest 1 Objective 1 Initialize");
    }

    protected override void EndObjective()
    {
        base.EndObjective();
        //If finish do this
        Debug.Log("Quest 1 Objective 1 Completed");
    }

    protected override bool CheckingObjective()
    {
        //Check Objective Condition here
        if (Input.GetKeyDown(KeyCode.Space))
        {
            return true;
        }
        return false;
    }
}
