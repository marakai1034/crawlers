using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum ObjectiveType
{
    Boolean,
    Float,
    Integer
}

public class ObjectiveIndicator
{
    public ObjectiveType objectiveType { get; private set; }

    private bool booleanIndicator = false;

    private float floatIndicator = 0;
    private float floatIndicatorMax = -1;

    private int integerIndicator = 0;
    private int integerIndicatorMax = -1;

    public void SetBooleanIndicator()
    {
        this.objectiveType = ObjectiveType.Boolean;
        booleanIndicator = false;
    }

    public void SetFloatIndicator(float floatIndicator, float floatIndicatorMax = -1)
    {
        this.objectiveType = ObjectiveType.Float;
        this.floatIndicator = floatIndicator;
        this.floatIndicatorMax = floatIndicatorMax;
    }

    public void SetIntegerIndicator(int integerIndicator, int integerIndicatorMax = -1)
    {
        this.objectiveType = ObjectiveType.Integer;
        this.integerIndicator = integerIndicator;
        this.integerIndicatorMax = integerIndicatorMax;
    }

    public override string ToString()
    {
        if (objectiveType == ObjectiveType.Integer)
        {
            if (integerIndicatorMax == -1)
            {
                return "(" + integerIndicatorMax.ToString() + ")";
            } else
            {
                return "(" + integerIndicator.ToString() + "/" + integerIndicatorMax.ToString() + ")";
            }
        } else if (objectiveType == ObjectiveType.Boolean)
        {
            return (booleanIndicator) ? "(Clear)" : "";
        } else if (objectiveType == ObjectiveType.Float)
        {
            if (floatIndicatorMax == -1)
            {
                return "(" + floatIndicatorMax.ToString() + ")";
            }
            else
            {
                return "(" + floatIndicator.ToString() + "/" + floatIndicatorMax.ToString() + ")";
            }
        }
        return "(0)";
    }
}

public abstract class Objective
{
    public string objectiveDesc { get; protected set; }
    public ObjectiveIndicator objectiveIndicator { get; protected set; }

    public bool isStarted { get; protected set; } = false;
    public bool isCompleted { get; protected set; } = false;

    protected virtual void InitializeObjective() //Event
    {
        isStarted = true;
    }

    protected virtual void EndObjective() //Action
    {
        isCompleted = true;
    }

    public void CheckObjective() //Process
    {
        if (!isStarted)
        {
            InitializeObjective();
        }
        else
        {
            if (!isCompleted)
            {
                if (CheckingObjective())
                {
                    EndObjective();
                }
            }
        }
    }

    protected virtual bool CheckingObjective()
    {
        return false;
    }
}

public abstract class Quest
{
    public string questName { get; protected set; }
    public string questDesc { get; protected set; }

    public bool isCompleted { get; protected set; } = false;

    protected List<Objective> objectiveList;
    public int objectiveIndex { get; protected set; } = 0;

    public void CheckQuest()
    {
        if (objectiveIndex < objectiveList.Count)
        {
            objectiveList[objectiveIndex].CheckObjective();

            if (objectiveList[objectiveIndex].isCompleted)
            {
                objectiveIndex++;
            }
        }
        else
        {
            isCompleted = true;
        }
    }
}
