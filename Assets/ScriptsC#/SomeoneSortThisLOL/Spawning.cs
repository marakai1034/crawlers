﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawning : MonoBehaviour
{
    public GameObject sub;
    float time;
    float timer;
    // Start is called before the first frame update
    void Start()
    {
        time = 30000 * Time.deltaTime;

    }

    // Update is called once per frame
    void Update()
    {
        timer++;
        if(timer >= time) {
            Vector3 position = new Vector3(Random.Range(-1000.0f, 1000.0f), Random.Range(-1000f, 1000f), Random.Range(-1000f, 1000f));
            Instantiate(sub, position, Quaternion.identity);
            timer = 0;
        }
    }
}
