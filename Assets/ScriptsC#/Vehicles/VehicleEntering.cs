using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VehicleEntering : MonoBehaviour
{
    public VehicleEnterable currentVehicle;

    public bool IsDriveVehicle { get { return currentVehicle != null; } }

    public void EnterVehicle(VehicleEnterable vehicle)
    {
        currentVehicle = vehicle;
        transform.parent = vehicle.transform;
    }

    public void ExitVehicle()
    {
        currentVehicle = null;
        transform.parent = null;
    }

    private void EnteringVehicle()
    {
        if (IsDriveVehicle) {
            transform.position = currentVehicle.cockpitPosition.position;
        }
    }

    public void Update()
    {
        EnteringVehicle();
    }
}
