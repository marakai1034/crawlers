﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnterVehicle : MonoBehaviour
{
    private bool inVehicle = false;
    GameObject player;
    VehicleMovement vehicleMovement;
    PlayerMovement playerMove;
    public GameObject Sub;
    public GameObject playerCam;
    public GameObject subCam;
    

    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindWithTag("Player");
        vehicleMovement = Sub.GetComponent<VehicleMovement>();
        playerMove = player.GetComponent<PlayerMovement>();
    }

    void OnTriggerStay(Collider other) {
        if(other.gameObject.tag == "Player" && inVehicle == false) {
            if (Input.GetKey(KeyCode.E)) {
                player.transform.parent = gameObject.transform;
              //  playerMove.controlled = false; 
                //player.SetActive(false);
                inVehicle = true;
                vehicleMovement.controlled = true;
                //subCam.SetActive(true);
                //playerCam.SetActive(false);

            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        if(inVehicle == true && Input.GetKey(KeyCode.T)) {
            //player.transform.rotation = Quaternion.identity;
           // playerMove.controlled = true;
            player.transform.parent = null;
            inVehicle = false;
            vehicleMovement.controlled = false;
            playerCam.SetActive(true);
            subCam.SetActive(false);

        }
    }
}
