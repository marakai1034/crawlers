﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VehicleCamera : MonoBehaviour
{
    private Rigidbody rb;

    public Vector3 cameraOffset;

    [SerializeField] private Transform vehicleTarget;
    [SerializeField] private Transform vehicleTargetCamera;
    [SerializeField] private Transform vehicleTargetCameraFront;

    public float movementRatio = 0.1f;
    public float rotationRatio = 0.1f;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    void Update()
    {
        if (vehicleTargetCamera)
        {
            Vector3 target = vehicleTarget.right * cameraOffset.x + Vector3.up * cameraOffset.y + vehicleTarget.forward * cameraOffset.z + vehicleTarget.position;

            rb.velocity = (target - transform.position).normalized * movementRatio * Vector3.Distance(target, transform.position);
            //rb.angularVelocity = (vehicleTargetCamera.rotation.eulerAngles - transform.rotation.eulerAngles).normalized * rotationRatio * Vector3.Distance(vehicleTargetCamera.rotation.eulerAngles, transform.rotation.eulerAngles);
            //transform.rotation = Quaternion.Lerp(transform.rotation, vehicleTargetCamera.transform.rotation, rotationRatio);
            transform.LookAt(vehicleTargetCameraFront);
        }
    }

    public void ResetCamera()
    {
        vehicleTargetCamera = null;
    }

    public void SetCamera(Transform targetCamera)
    {
        vehicleTargetCamera = targetCamera;
    }
}
