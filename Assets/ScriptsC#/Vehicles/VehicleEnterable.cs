using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VehicleEnterable : MonoBehaviour
{
    public Transform cockpitPosition;

    private void OnTriggerStay(Collider other)
    {
        if (Input.GetKeyDown(GlobalKeyManager.ENTER_VEHICLE))
        {
            if (other.tag == "Player")
            {
                VehicleEntering player = other.GetComponent<VehicleEntering>();
                if (!player.IsDriveVehicle)
                {
                    player.EnterVehicle(this);
                }
                else if (player.IsDriveVehicle == this)
                {
                    player.ExitVehicle();
                }
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            VehicleEntering player = other.GetComponent<VehicleEntering>();
            if (player.IsDriveVehicle == this)
            {
                player.ExitVehicle();
            }
        }
    }
}
