using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Tool like Flash Light, Cutting tools, Welder come here
/// </summary>
[CreateAssetMenu(fileName = "New Tool", menuName = "New Inventory System/Inventory/New Tool")]
public class InventoryTool : InventoryUnconsumable
{
    public ToolType toolType;

    public int value; //some damage, value or power comes here.

    public List<InventoryAttachment> toolAttachments;
}