using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InventoryItem : ScriptableObject
{
    [Header("This is unique Item ID,")]
    [Header("Eg : Important only to Consumables")]
    public int itemID;
    public string itemName;

    [TextArea()]
    public string itemDesc;
}