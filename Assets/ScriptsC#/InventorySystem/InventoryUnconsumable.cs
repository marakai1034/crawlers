using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Inventory Unconsumable, like Tools and Weapons
/// </summary>
public class InventoryUnconsumable : InventoryItem
{

}