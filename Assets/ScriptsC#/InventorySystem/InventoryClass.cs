using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum EdibleType
{
    //Food
    Apple,
    Mango,

    //Drink
    Water,
    Coconut
}

public enum NonEdibleType
{
    //Ammo
    PistolAmmo,
    ShotgunAmmo,
    RifleAmmo,
    HeavyWeaponAmmo,
    ExplosiveAmmo,

    //Fuel
    Fuel,

    //Battery
    Battery,

    //Scrap
    Scrap,

    //Parts
    Part
}

public enum ToolType
{
    CuttingTools,
    Torch,
    Grinder,
    Jigsaw,
    Welder,
    Radar,
    ThermalImaging,
    Flashlight
}

public enum WeaponType
{
    Pistol,
    Shotgun,
    Rifle,
    HeavyWeapon,
    Explosive
}

public enum WeaponMode
{
    //Pistols
    PAutomatic,
    PSemiAutomatic,
    PRevolver,

    //Shotguns
    SBreakAction,
    SPumpAction,
    SLeverAction,
    SSemiAutomatic,

    //Rifles
    RSemiAutomatic,
    RLeverAction,
    RBoltAction,
    RAutomatic,

    //Heavy Weapons
    HWChainGun,
    HWFlamethrower,
    HWRocketLauncher,

    //Explosives
    EXDynamite,
    EXc4,
    EXGrenade,
    EXLandmine,
    EXClaymore
}

public enum ToolAttachmentType
{
    //Idk
}

public enum WeaponAttachmentType
{
    //Idk
}