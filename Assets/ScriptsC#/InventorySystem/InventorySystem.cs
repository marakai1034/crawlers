using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InventorySystem : MonoBehaviour
{
    [SerializeField] private List<InventoryContainer> containers;

    public void AddItem(InventoryItem item)
    {
        //container.Add(item);
        if (item is InventoryConsumable)
        {
            AddConsumableItem(item);
        } else if (item is InventoryUnconsumable)
        {
            AddUnconsumableItem(item);
        }
    }

    public void AddConsumableItem(InventoryItem item)
    {
        InventoryContainer container = GetItemFromID(item.itemID);
        if (container == null)
        {
            InventoryContainer newContainer = new InventoryContainer(item);
            containers.Add(newContainer);
        } else
        {
            container.AddItem();
        }
    }

    public void AddUnconsumableItem(InventoryItem item)
    {
        InventoryContainer container = new InventoryContainer(item);
        containers.Add(container);
    }

    public InventoryContainer GetItemFromIndex(int index)
    {
        if (index >= 0 && index < containers.Count)
        {
            InventoryContainer output = containers[index];
            return output;
        }

        return null;
    }

    public InventoryContainer GetItemFromID(int itemID)
    {
        foreach (InventoryContainer container in containers)
        {
            if (container.GetItem().itemID == itemID)
            {
                return container;
            }
        }
        return null;
    }

    public List<InventoryContainer> GetAllItems()
    {
        return containers;
    }

    public void RemoveItemFromIndex(int index)
    {
        if (index >= 0 && index < containers.Count)
        {
            containers[index].RemoveItem();

            if (containers[index].Count == 0)
            {
                containers[index] = null;
            }
        }
    }

    public void RemoveAllItemFromIndex(int index)
    {
        if (index >= 0 && index < containers.Count)
        {
            containers[index] = null;
        }
    }

    public void ClearItems()
    {
        containers.Clear();
    }
}

[System.Serializable]
public class InventoryContainer
{
    [SerializeField]
    private InventoryItem item;
    public int Count { get; private set; } = 0;

    public InventoryContainer(InventoryItem item)
    {
        this.item = item;
        Count = 1;
    }

    public void AddItem()
    {
        Count++;
    }

    public void RemoveItem()
    {
        if (Count > 0)
        {
            Count--;
        }
    }

    public InventoryItem GetItem()
    {
        return item;
    }
}