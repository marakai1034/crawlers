using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Weapon like Shotgun, smg, etc, come here
/// </summary>
[CreateAssetMenu(fileName = "New Weapon", menuName = "New Inventory System/Inventory/New Weapon")]
public class InventoryWeapon : InventoryUnconsumable
{
    public WeaponType weaponType;

    //[Header("P : Pistols")]
    //[Header("S : Shotguns")]
    //[Header("R : Rifles")]
    //[Header("HW : HeavyWeapons")]
    //[Header("EX : Explosives")]

    [Header("Hover cursor on [Weapon Mode], for explanation")]

    [Tooltip("P : Pistols, S : Shotguns, R : Rifles, HW : HeavyWeapons, EX : Explosives")]
    public WeaponMode weaponMode;

    public List<InventoryAttachment> weaponAttachments;

    public float range;
    [Range(0.0f, 1.0f)] public float accuracy;

    public float damage;
    public float rateOfFire;
    public int magazineSize;
}