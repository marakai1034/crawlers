using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Ammo, Fuel and Not Edible things goes here.
/// </summary>
[CreateAssetMenu(fileName = "New NonEdible Item", menuName = "New Inventory System/Inventory/New Non Edible")]
public class InventoryNonEdible : InventoryConsumable
{
    [Header("Ammo, Fuel and Not Edible things goes here.")]
    public NonEdibleType nonEdibleType;

    public int value;

    //Example : Shotgun Ammo with 100 magazine. Fuel with 40 power.. etc..
}