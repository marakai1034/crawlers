using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Weapon Attachment", menuName = "New Inventory System/Inventory/New Attachment/Weapon Attachment")]
public class InventoryWeaponAttachment : InventoryAttachment
{
    public float rangeBuff;
    public float accuracyBuff;

    public float damageBuff;
    public float rateOfFireBuff;
    public int magazineSizeBuff;

    //public bool autoHeadshot ?, idk.
}
