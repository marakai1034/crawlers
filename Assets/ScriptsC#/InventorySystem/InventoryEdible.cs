using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Food and Drink items, come here
/// </summary>
[CreateAssetMenu(fileName = "New Edible Item", menuName = "New Inventory System/Inventory/New Edible")]
public class InventoryEdible : InventoryConsumable
{
    [Header("Food and Drink items, come here")]
    public EdibleType edibleType;

    //Buff

    public float healthBuff;
    public float staminaBuff;

    public float hungerBuff;
    public float hydrateBuff;
    
    //public float etcccc...
}